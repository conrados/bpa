	$( document ).ready(function() {
		$("a[href*=#]").on('click', function(event){
		    var href = $(this).attr("href");
		    if ( /(#.*)/.test(href) ){
		      var hash = href.match(/(#.*)/)[0];
		      var path = href.match(/([^#]*)/)[0];

		      if (window.location.pathname == path || path.length == 0){
		        event.preventDefault();
		        if ($('.headerMobile').hasClass('nav-up')) {
		        	$('html,body').animate({scrollTop:$(this.hash).offset().top}, 900);
		        	console.log('-50 nav up');
		        } else if ($('.headerMobile').hasClass('nav-down')) {
		        	$('html,body').animate({scrollTop:$(this.hash).offset().top-50}, 900);
		        	console.log('0 nav nav-down');
		        }
		        
		        window.location.hash = hash;
		      }
		    }
		});
	});